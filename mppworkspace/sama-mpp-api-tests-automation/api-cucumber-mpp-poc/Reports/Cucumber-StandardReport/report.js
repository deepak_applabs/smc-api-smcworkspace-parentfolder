$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("./src/test/resources/com/sama/mpp/cucumber/features/001_MPP_API_Create registration with single proxy for individual account.feature");
formatter.feature({
  "name": "001_MPP_API_Create registration with single proxy for individual account",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "01_06_MPP_API_Business error ( Wrong Proxy Type ) cases of Registration API for creation of Registration",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@01_08_CreateReg_WrongProxyType"
    }
  ]
});
formatter.step({
  "name": "a REST API request \"\u003crequestFile\u003e\" and url \"\u003cencyptionEndpoint\u003e\" along with below headers under \"%ENV%\" environment",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "accept",
        "\u003caccept\u003e"
      ]
    },
    {
      "cells": [
        "content-type",
        "\u003ccontent type\u003e"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "\u003cx_ibm_client_id\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "\"POST\" action is performed",
  "keyword": "When "
});
formatter.step({
  "name": "the status code is \"\u003cstatusCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "keyword": "And ",
  "rows": [
    {
      "cells": [
        "encryptdata",
        "\u003cEncrypted_Text\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "a encrypted output  \"\u003ccipherdata\u003e\" and url \"\u003ctokenEndpoint\u003e\" along with below headers under \"%ENV%\" environment",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "accept",
        "\u003caccept\u003e"
      ]
    },
    {
      "cells": [
        "content-type",
        "\u003ccontent type\u003e"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "\u003cx_ibm_client_id\u003e"
      ]
    },
    {
      "cells": [
        "x-ibm-client-secret",
        "\u003cx_ibm_client_secret\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "\"POST\" action is performed to genarate \"encypted\" access token",
  "keyword": "When "
});
formatter.step({
  "name": "the status code is \"\u003cstatusCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "keyword": "And ",
  "rows": [
    {
      "cells": [
        "cipheredData",
        "\u003cEncrypted_Text\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "a encrypted output  \"\u003ccipherdata1\u003e\" and url \"\u003cdecyptionEndpoint\u003e\" along with below headers under \"%ENV%\" environment",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "accept",
        "\u003caccept\u003e"
      ]
    },
    {
      "cells": [
        "content-type",
        "\u003ccontent type\u003e"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "\u003cx_ibm_client_id\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "\"POST\" action is performed to genarate \"decypted\" access token",
  "keyword": "When "
});
formatter.step({
  "name": "the status code is \"\u003cstatusCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "keyword": "And ",
  "rows": [
    {
      "cells": [
        "token_type",
        "\u003ctoken_type\u003e"
      ]
    },
    {
      "cells": [
        "access_token",
        "\u003caccess_token\u003e"
      ]
    },
    {
      "cells": [
        "expires_in",
        "\u003cexpires_in\u003e"
      ]
    },
    {
      "cells": [
        "consented_on",
        "\u003cconsented_on\u003e"
      ]
    },
    {
      "cells": [
        "scope",
        "\u003cscope\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "a REST API request \"\u003csignatureRequestFile\u003e\" and url \"\u003csignatureEndpoint\u003e\" along with below headers under \"%ENV%\" environment",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "accept",
        "\u003caccept\u003e"
      ]
    },
    {
      "cells": [
        "content-type",
        "\u003ccontent type\u003e"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "\u003cx_ibm_client_id\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "\"POST\" action is performed to genarate signature for \"Proxy Registration\"",
  "keyword": "When "
});
formatter.step({
  "name": "the status code is \"\u003cstatusCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "keyword": "And ",
  "rows": [
    {
      "cells": [
        "signature",
        "\u003cEncrypted_Text\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "a REST API request \"\u003csignatureRequestFile\u003e\" and url \"\u003cregistrationEndpoint\u003e\" along with below headers under \"%ENV%\" environment",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "accept",
        "\u003caccept\u003e"
      ]
    },
    {
      "cells": [
        "authorization",
        "\u003cautorization\u003e"
      ]
    },
    {
      "cells": [
        "content-type",
        "\u003ccontent type\u003e"
      ]
    },
    {
      "cells": [
        "signature",
        "\u003csignature\u003e"
      ]
    },
    {
      "cells": [
        "participant-code",
        "\u003cparticipant-code\u003e"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "\u003cx_ibm_client_id\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "\"POST\" action is performed to genarate \"Proxy Registration\"",
  "keyword": "When "
});
formatter.step({
  "name": "the status code is \"\u003cstatusCode1\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "keyword": "And ",
  "rows": [
    {
      "cells": [
        "ReasonCode",
        "\u003cReasonCode\u003e"
      ]
    },
    {
      "cells": [
        "Description",
        "\u003cDescription\u003e"
      ]
    }
  ]
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "requestFile",
        "signatureRequestFile",
        "encyptionEndpoint",
        "tokenEndpoint",
        "decyptionEndpoint",
        "signatureEndpoint",
        "registrationEndpoint",
        "statusCode",
        "statusCode1",
        "statusCode2",
        "accept",
        "content type",
        "x_ibm_client_id",
        "x_ibm_client_secret",
        "Encrypted_Text",
        "token_type",
        "access_token",
        "expires_in",
        "consented_on",
        "scope",
        "autorization",
        "signature",
        "participant-code",
        "ReasonCode",
        "Description"
      ]
    },
    {
      "cells": [
        "requestEncryption",
        "01_08_generateSignature",
        "/sp/dev/dummy/encrypt",
        "/sp/dev/v1/token/generation",
        "/sp/dev/dummy/decrypt",
        "/sp/dev/dummy/generate",
        "/sp/sit/v1/customer-proxy-registrations/registrations",
        "200",
        "400",
        "204",
        "application/json",
        "application/json",
        "02a18293-3e8b-4b90-9456-4cb291734386",
        "W6pR1aA0gX4aM2gC4aT5yU6xI8pV7nL3aM1pU8aL7oQ3cN7uC4",
        "%NOTEMPTY%",
        "Bearer",
        "%NOTEMPTY%",
        "3600",
        "%NOTEMPTY%",
        "ips",
        "%RUNTIME%",
        "%RUNTIME%",
        "SANCBK",
        "802",
        "Proxy Type is not valid"
      ]
    }
  ]
});
formatter.scenario({
  "name": "01_06_MPP_API_Business error ( Wrong Proxy Type ) cases of Registration API for creation of Registration",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@01_08_CreateReg_WrongProxyType"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "a REST API request \"requestEncryption\" and url \"/sp/dev/dummy/encrypt\" along with below headers under \"%ENV%\" environment",
  "rows": [
    {
      "cells": [
        "accept",
        "application/json"
      ]
    },
    {
      "cells": [
        "content-type",
        "application/json"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "02a18293-3e8b-4b90-9456-4cb291734386"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "POC_StepDefinitions.a_REST_API_request_and_url_along_with_below_headers_under_environment(String,String,String,String\u003e\u003e)"
});
formatter.write("Required Headers values for URL :: https://10.158.244.98/sp/dev/dummy/encrypt  :: {\r\n  \"x-ibm-client-id\" : \"02a18293-3e8b-4b90-9456-4cb291734386\",\r\n  \"content-type\" : \"application/json\",\r\n  \"accept\" : \"application/json\"\r\n}");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "\"POST\" action is performed",
  "keyword": "When "
});
formatter.match({
  "location": "POC_StepDefinitions.action_is_performed(String)"
});
formatter.result({
  "error_message": "java.net.ConnectException: Connection timed out: connect\r\n\tat java.net.DualStackPlainSocketImpl.connect0(Native Method)\r\n\tat java.net.DualStackPlainSocketImpl.socketConnect(DualStackPlainSocketImpl.java:79)\r\n\tat java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:350)\r\n\tat java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:206)\r\n\tat java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:188)\r\n\tat java.net.PlainSocketImpl.connect(PlainSocketImpl.java:172)\r\n\tat java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392)\r\n\tat java.net.Socket.connect(Socket.java:589)\r\n\tat org.apache.http.conn.ssl.SSLSocketFactory.connectSocket(SSLSocketFactory.java:543)\r\n\tat org.apache.http.conn.ssl.SSLSocketFactory.connectSocket(SSLSocketFactory.java:415)\r\n\tat org.apache.http.impl.conn.DefaultClientConnectionOperator.openConnection(DefaultClientConnectionOperator.java:180)\r\n\tat org.apache.http.impl.conn.ManagedClientConnectionImpl.open(ManagedClientConnectionImpl.java:326)\r\n\tat org.apache.http.impl.client.DefaultRequestDirector.tryConnect(DefaultRequestDirector.java:605)\r\n\tat org.apache.http.impl.client.DefaultRequestDirector.execute(DefaultRequestDirector.java:440)\r\n\tat org.apache.http.impl.client.AbstractHttpClient.doExecute(AbstractHttpClient.java:835)\r\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:83)\r\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:56)\r\n\tat org.apache.http.client.HttpClient$execute$0.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:148)\r\n\tat io.restassured.internal.RequestSpecificationImpl$RestAssuredHttpBuilder.doRequest(RequestSpecificationImpl.groovy:2055)\r\n\tat io.restassured.internal.http.HTTPBuilder.post(HTTPBuilder.java:350)\r\n\tat io.restassured.internal.http.HTTPBuilder$post$2.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:148)\r\n\tat io.restassured.internal.RequestSpecificationImpl.sendRequest(RequestSpecificationImpl.groovy:1181)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\r\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1259)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1026)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:812)\r\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:166)\r\n\tat io.restassured.internal.filter.SendRequestFilter.filter(SendRequestFilter.groovy:30)\r\n\tat io.restassured.filter.Filter$filter$0.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat io.restassured.filter.Filter$filter.call(Unknown Source)\r\n\tat io.restassured.internal.filter.FilterContextImpl.next(FilterContextImpl.groovy:72)\r\n\tat io.restassured.filter.time.TimingFilter.filter(TimingFilter.java:56)\r\n\tat io.restassured.filter.Filter$filter.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:157)\r\n\tat io.restassured.internal.filter.FilterContextImpl.next(FilterContextImpl.groovy:72)\r\n\tat io.restassured.filter.FilterContext$next.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:47)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:148)\r\n\tat io.restassured.internal.RequestSpecificationImpl.applyPathParamsAndSendRequest(RequestSpecificationImpl.groovy:1655)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\r\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1259)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1026)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:812)\r\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.callCurrent(PogoInterceptableSite.java:55)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCallCurrent(CallSiteArray.java:51)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:171)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:203)\r\n\tat io.restassured.internal.RequestSpecificationImpl.applyPathParamsAndSendRequest(RequestSpecificationImpl.groovy:1661)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.codehaus.groovy.reflection.CachedMethod.invoke(CachedMethod.java:107)\r\n\tat groovy.lang.MetaMethod.doMethodInvoke(MetaMethod.java:323)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1259)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:1026)\r\n\tat groovy.lang.MetaClassImpl.invokeMethod(MetaClassImpl.java:812)\r\n\tat groovy.lang.GroovyObject.invokeMethod(GroovyObject.java:39)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.call(PogoInterceptableSite.java:45)\r\n\tat org.codehaus.groovy.runtime.callsite.PogoInterceptableSite.callCurrent(PogoInterceptableSite.java:55)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCallCurrent(CallSiteArray.java:51)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:171)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.callCurrent(AbstractCallSite.java:203)\r\n\tat io.restassured.internal.RequestSpecificationImpl.post(RequestSpecificationImpl.groovy:175)\r\n\tat io.restassured.internal.RequestSpecificationImpl.post(RequestSpecificationImpl.groovy)\r\n\tat com.sama.mpp.cucumber.poc.utils.SamaMPPLib.requestActivity(SamaMPPLib.java:272)\r\n\tat com.sama.mpp.cucumber.poc.stepdefs.POC_StepDefinitions.action_is_performed(POC_StepDefinitions.java:75)\r\n\tat ✽.\"POST\" action is performed(./src/test/resources/com/sama/mpp/cucumber/features/001_MPP_API_Create registration with single proxy for individual account.feature:679)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "the status code is \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "POC_StepDefinitions.the_status_code_is(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "rows": [
    {
      "cells": [
        "encryptdata",
        "%NOTEMPTY%"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "POC_StepDefinitions.response_includes_the_following_values_as(String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "a encrypted output  \"\u003ccipherdata\u003e\" and url \"/sp/dev/v1/token/generation\" along with below headers under \"%ENV%\" environment",
  "rows": [
    {
      "cells": [
        "accept",
        "application/json"
      ]
    },
    {
      "cells": [
        "content-type",
        "application/json"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "02a18293-3e8b-4b90-9456-4cb291734386"
      ]
    },
    {
      "cells": [
        "x-ibm-client-secret",
        "W6pR1aA0gX4aM2gC4aT5yU6xI8pV7nL3aM1pU8aL7oQ3cN7uC4"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "POC_StepDefinitions.a_encrypted_output_and_url_along_with_below_headers_under_environment(String,String,String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"POST\" action is performed to genarate \"encypted\" access token",
  "keyword": "When "
});
formatter.match({
  "location": "POC_StepDefinitions.action_is_performed_to_genarate_access_token(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the status code is \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "POC_StepDefinitions.the_status_code_is(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "rows": [
    {
      "cells": [
        "cipheredData",
        "%NOTEMPTY%"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "POC_StepDefinitions.response_includes_the_following_values_as(String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "a encrypted output  \"\u003ccipherdata1\u003e\" and url \"/sp/dev/dummy/decrypt\" along with below headers under \"%ENV%\" environment",
  "rows": [
    {
      "cells": [
        "accept",
        "application/json"
      ]
    },
    {
      "cells": [
        "content-type",
        "application/json"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "02a18293-3e8b-4b90-9456-4cb291734386"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "POC_StepDefinitions.a_encrypted_output_and_url_along_with_below_headers_under_environment(String,String,String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"POST\" action is performed to genarate \"decypted\" access token",
  "keyword": "When "
});
formatter.match({
  "location": "POC_StepDefinitions.action_is_performed_to_genarate_access_token(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the status code is \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "POC_StepDefinitions.the_status_code_is(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "rows": [
    {
      "cells": [
        "token_type",
        "Bearer"
      ]
    },
    {
      "cells": [
        "access_token",
        "%NOTEMPTY%"
      ]
    },
    {
      "cells": [
        "expires_in",
        "3600"
      ]
    },
    {
      "cells": [
        "consented_on",
        "%NOTEMPTY%"
      ]
    },
    {
      "cells": [
        "scope",
        "ips"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "POC_StepDefinitions.response_includes_the_following_values_as(String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "a REST API request \"01_08_generateSignature\" and url \"/sp/dev/dummy/generate\" along with below headers under \"%ENV%\" environment",
  "rows": [
    {
      "cells": [
        "accept",
        "application/json"
      ]
    },
    {
      "cells": [
        "content-type",
        "application/json"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "02a18293-3e8b-4b90-9456-4cb291734386"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "POC_StepDefinitions.a_REST_API_request_and_url_along_with_below_headers_under_environment(String,String,String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"POST\" action is performed to genarate signature for \"Proxy Registration\"",
  "keyword": "When "
});
formatter.match({
  "location": "POC_StepDefinitions.action_is_performed_to_genarate_signature_for(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the status code is \"200\"",
  "keyword": "Then "
});
formatter.match({
  "location": "POC_StepDefinitions.the_status_code_is(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "rows": [
    {
      "cells": [
        "signature",
        "%NOTEMPTY%"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "POC_StepDefinitions.response_includes_the_following_values_as(String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "a REST API request \"01_08_generateSignature\" and url \"/sp/sit/v1/customer-proxy-registrations/registrations\" along with below headers under \"%ENV%\" environment",
  "rows": [
    {
      "cells": [
        "accept",
        "application/json"
      ]
    },
    {
      "cells": [
        "authorization",
        "%RUNTIME%"
      ]
    },
    {
      "cells": [
        "content-type",
        "application/json"
      ]
    },
    {
      "cells": [
        "signature",
        "%RUNTIME%"
      ]
    },
    {
      "cells": [
        "participant-code",
        "SANCBK"
      ]
    },
    {
      "cells": [
        "x-ibm-client-id",
        "02a18293-3e8b-4b90-9456-4cb291734386"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "POC_StepDefinitions.a_REST_API_request_and_url_along_with_below_headers_under_environment(String,String,String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"POST\" action is performed to genarate \"Proxy Registration\"",
  "keyword": "When "
});
formatter.match({
  "location": "POC_StepDefinitions.action_is_performed_to_genarate(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "the status code is \"400\"",
  "keyword": "Then "
});
formatter.match({
  "location": "POC_StepDefinitions.the_status_code_is(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "\"Json\" response includes the following values as",
  "rows": [
    {
      "cells": [
        "ReasonCode",
        "802"
      ]
    },
    {
      "cells": [
        "Description",
        "Proxy Type is not valid"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "POC_StepDefinitions.response_includes_the_following_values_as(String,String\u003e\u003e)"
});
formatter.result({
  "status": "skipped"
});
});